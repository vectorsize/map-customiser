# Map customizer

This is a test to build a map customization tool in vanilla javascript, the goal was to put together
a minimal application without using any application library such as React/Redux or Backbone.  

For the map I chose [leaflet](http://leafletjs.com/), and for the application architecture I went for a **Component based** architecture.

## Component based architecture

The whole application is built around Components, with a built-in reactive state management.  
Components inherit from a base [Component](https://gitlab.com/vectorsize/map-customizer/blob/master/public/lib/component.js) Prototype and will override separate methods when needed to handle rendering, DOM basic structure and interactions.  
The component is loosely modeled after [React components](https://facebook.github.io/react/) and their internal state management.

## Remarks

The Component implementation is a light weight implementation, some features and optimizations are missing.  

The applications could have addressed visualization aspects such as reflecting the actual population with color/opacity/sizes,  
but given the limited amount of time I had to put into this I decided to focus more on the editing capabilities of the tool,  
and set the right state management workflow.

The sliders interaction only affects the map display after the slider is released. This is due to the fact that the map became slightly irresponsive given the amount of data points.  
Had I had more time I would have addressed this by clustering data points based on the map zoom level, or include population ranges as a filter to display less data simultaneously.

The layout is mostly for desktop screens, it is pseudo-responsive at the moment but doesn't have any breakpoints or any particular effort to work in devices.
