(function(exports) {
  var Component = exports.Component;
  var scale = exports.scale;

  function MapComponent(options, initialState) {
    Component.call(this, options, initialState);
    this.data = initialState.data;
    this.points = [];
  }

  MapComponent.prototype = Object.create(Component.prototype);

  MapComponent.prototype.appendTo = function(parent) {
    parent.append(this.$element);
    this.renderMap();
    this.renderData();
  };

  MapComponent.prototype.renderMap = function() {
    this.map = new L.Map(this.$element).setView([0, 0], 3);
    var tiles = new L.TileLayer(
      'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    );

    tiles.addTo(this.map);
  };

  MapComponent.prototype.renderData = function() {
    var data = this.data;
    var self = this;
    var maxs = [];
    var mins = [];
    this.dotLayer = L.geoJSON(data, {
      pointToLayer: function(feature, latlng) {
        var point = L.circleMarker(latlng);
        maxs.push(feature.properties.pop_max);
        mins.push(feature.properties.pop_min);
        self.points.push(point);
        return point;
      }
    });
    // console.log([0, 1], [min, max]);
    this.min = Math.max.apply(Math, mins);
    this.max = Math.max.apply(Math, maxs);
    this.scale = scale([0, 1], [this.min, this.max]);
  };

  MapComponent.prototype.render = function(newState) {
    var map = this.map;
    var style = newState.style;
    // var radius = style.radius

    var showHideDots = newState.dotsVisible ? 'addTo' : 'removeFrom';
    this.dotLayer[showHideDots](map);
    var act = newState.panelOpen ? 'add' : 'remove';
    this.$element.classList[act]('panelOpen');

    // update style
    this.points.forEach(function(point) {
      var pmin = point.feature.properties.pop_min;
      var pmax = point.feature.properties.pop_max;
      var population = (pmax - pmin) / 2;
      point.setStyle(style);
    });
  };

  exports.MapComponent = MapComponent;
})(window.App);
