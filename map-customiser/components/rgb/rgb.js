(function(exports) {
  var Component = exports.Component;
  var Slider = exports.Slider;
  var Group = exports.Group;

  function Rgb(options, initialState) {
    Component.call(this, options, initialState);
  }

  Rgb.prototype = Object.create(Component.prototype);

  Rgb.prototype.setup = function() {
    this.$preview = document.createElement('div');
    this.$preview.className = 'rgb-preview';
    this.$labelContainer = document.createElement('div');
    this.$labelContainer.className = 'rgb-preview-container';
    this.$label = document.createElement('div');
    this.$label.innerHTML = this.options.label;

    this.$rgb = new Group({
      className: 'group color-group'
    });

    this.$red = new Slider(
      Object.assign(
        { className: 'red slider', label: 'red' },
        { range: this.options.range, round: this.options.round }
      ),
      { value: this.state.red }
    );

    this.$green = new Slider(
      Object.assign(
        { className: 'green slider', label: 'green' },
        { range: this.options.range, round: this.options.round }
      ),
      { value: this.state.green }
    );

    this.$blue = new Slider(
      Object.assign(
        { className: 'blue slider', label: 'blue' },
        { range: this.options.range, round: this.options.round }
      ),
      { value: this.state.blue }
    );

    this.mountElements();
    this.bindEvents();
  };

  Rgb.prototype.mountElements = function() {
    var container = this.$element;
    var labelContainer = this.$labelContainer;
    var label = this.$label;
    var preview = this.$preview;
    var rgb = this.$rgb;
    var red = this.$red;
    var green = this.$green;
    var blue = this.$blue;

    var rgbNode = rgb.node();
    rgb.appendTo(container);
    rgbNode.append(labelContainer);
    labelContainer.append(label);
    labelContainer.append(preview);
    red.appendTo(rgbNode);
    green.appendTo(rgbNode);
    blue.appendTo(rgbNode);
  };

  Rgb.prototype.on = function(action, callback) {
    if (action === 'input' || action === 'change') {
      this.$red.$element.addEventListener(action, callback);
      this.$green.$element.addEventListener(action, callback);
      this.$blue.$element.addEventListener(action, callback);
    }
  };

  Rgb.prototype.value = function() {
    return this.state;
  };

  Rgb.prototype.bindEvents = function() {
    this.$red.on('input', () => {
      var red = this.$red.value();
      this.setState({ red });
    });
    this.$green.on('input', () => {
      var green = this.$green.value();
      this.setState({ green });
    });
    this.$blue.on('input', () => {
      var blue = this.$blue.value();
      this.setState({ blue });
    });
  };

  Rgb.prototype.render = function(newState) {
    var rgb = [newState.red, newState.green, newState.blue];
    this.$preview.style.backgroundColor = 'rgb(' + rgb.join(',') + ')';
    this.$red.render({ value: rgb[0] });
    this.$green.render({ value: rgb[1] });
    this.$blue.render({ value: rgb[2] });
  };

  exports.Rgb = Rgb;
})(window.App);
