(function(exports) {
  // source https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
  exports.toHex = rgb =>
    '#' +
    ((1 << 24) + (rgb[0] << 16) + (rgb[1] << 8) + rgb[2]).toString(16).slice(1);
  exports.scale = function(domain, range) {
    var istart = domain[0],
      istop = domain[1],
      ostart = range[0],
      ostop = range[1];

    return function scale(value) {
      return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
    };
  };
})(window.App);
