(function(exports) {
  var Component = exports.Component;
  var Slider = exports.Slider;
  var Group = exports.Group;

  function RangeComponent(options, initialState) {
    Component.call(this, options, initialState);
  }

  RangeComponent.prototype = Object.create(Component.prototype);

  RangeComponent.prototype.setup = function() {
    this.$rangeGroup = new Group({
      id: 'range-group',
      className: 'group',
      label: this.options.label || 'Range'
    });

    this.$min = new Slider(
      { className: 'min slider', label: 'min' },
      { value: this.state.min }
    );

    this.$max = new Slider(
      { className: 'max slider', label: 'max' },
      { value: this.state.max }
    );

    this.mountElements();
    this.bindEvents();
  };

  RangeComponent.prototype.mountElements = function() {
    var container = this.$element;
    var rangeGroup = this.$rangeGroup;
    var min = this.$min;
    var max = this.$max;

    var rangeGroupNode = rangeGroup.node();
    rangeGroup.appendTo(container);
    min.appendTo(rangeGroupNode);
    max.appendTo(rangeGroupNode);
  };

  RangeComponent.prototype.on = function(action, callback) {
    if (action === 'input' || action === 'change') {
      this.$min.$element.addEventListener(action, callback);
      this.$max.$element.addEventListener(action, callback);
    }
  };

  RangeComponent.prototype.value = function() {
    return this.state;
  };

  RangeComponent.prototype.bindEvents = function() {
    this.$min.on('input', () => {
      var min = this.$min.value();
      this.setState({ min });
    });
    this.$max.on('input', () => {
      var max = this.$max.value();
      this.setState({ max });
    });
  };

  RangeComponent.prototype.render = function(newState) {
    this.$min.render({ value: this.state.min });
    this.$max.render({ value: this.state.max });
  };

  exports.RangeComponent = RangeComponent;
})(window.App);
