(function(exports) {
  var Component = exports.Component;

  function Panel(options, initialState) {
    Component.call(this, options, initialState);
  }

  Panel.prototype = Object.create(Component.prototype);

  Panel.prototype.setup = function(newState) {
    if (this.options.label) {
      this.$label = document.createElement('div');
      this.$label.className = 'label';
      this.$label.innerHTML = this.options.label;
      this.$element.append(this.$label);
    }
  };

  Panel.prototype.render = function(newState) {
    var act = newState.open ? 'remove' : 'add';
    this.$element.classList[act]('closed');
  };

  exports.Panel = Panel;
})(window.App);
