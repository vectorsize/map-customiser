(function(exports) {
  function Component(options, initialState) {
    this.state = initialState || {};
    this.className = options.className || 'component';
    this.id = options.id || 'comp-' + +new Date();
    this.options = options;
    this.hasEventListener = false;
    this.createElement();
  }

  Component.prototype.createElement = function() {
    this.$element = document.createElement('div');
    this.$element.id = this.id;
    this.$element.className = this.className;
    if (this.setup) this.setup();
  };

  Component.prototype.appendTo = function($parent) {
    if (!this.$element) this.createElement();
    $parent = $parent || document.body;
    $parent.append(this.$element);
    this.render(this.state);
  };

  Component.prototype.setState = function update(newState) {
    this.state = Object.assign({}, this.state, newState);
    this.render(this.state);
  };

  Component.prototype.render = function(newState) {};

  Component.prototype.on = function(action, callback) {
    if (!this.hasEventListener) {
      this.emiter().addEventListener(action, callback);
      this.hasEventListener = true;
    }
  };

  Component.prototype.emiter = function() {
    return this.node();
  };

  Component.prototype.value = function() {
    return this.emiter().value;
  };

  Component.prototype.node = function() {
    return this.$element;
  };

  exports.Component = Component;
})(window.App);
