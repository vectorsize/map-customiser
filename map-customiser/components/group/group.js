(function(exports) {
  var Component = exports.Component;

  function Group(options, initialState) {
    Component.call(this, options, initialState);
  }

  Group.prototype = Object.create(Component.prototype);

  Group.prototype.setup = function() {
    this.$element.className = (this.options.className || '') + ' group';
    if (this.options.label) {
      this.$label = document.createElement('div');
      this.$label.className = 'label';
      this.$label.innerHTML = this.options.label;
      this.$element.append(this.$label);
    }
  };

  exports.Group = Group;
})(window.App);
