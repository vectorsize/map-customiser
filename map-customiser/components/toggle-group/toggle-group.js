(function(exports) {
  var Component = exports.Component;
  var Toggle = exports.Toggle;

  function ToggleGroup(options, initialState) {
    Component.call(this, options, initialState);
  }

  ToggleGroup.prototype = Object.create(Component.prototype);

  ToggleGroup.prototype.setup = function() {
    if (this.options.label) {
      this.$label = document.createElement('div');
      this.$label.className = 'label';
      this.$label.innerHTML = this.options.label;
      this.$element.append(this.$label);
    }

    this.$toggle = new Toggle(
      { className: 'toggle', label: ' ' },
      { active: this.state.panelOpen }
    );

    this.$toggle.appendTo(this.$element);
  };

  ToggleGroup.prototype.emiter = function() {
    return this.$toggle.node();
  };

  ToggleGroup.prototype.render = function(newState) {
    if (newState.label !== this.options.label) {
      this.options.label = newState.label;
      this.$label.innerHTML = this.options.label;
    }
    var act = newState.active ? 'add' : 'remove';
    this.$toggle.node().classList[act]('active');
  };

  exports.ToggleGroup = ToggleGroup;
})(window.App);
