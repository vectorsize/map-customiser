(function(exports) {
  var toHex = exports.toHex;

  var Component = exports.Component;
  var MapComponent = exports.MapComponent;
  var Toolbar = exports.Toolbar;
  var Group = exports.Group;
  var Panel = exports.Panel;
  var Toggle = exports.Toggle;
  var ToggleGroup = exports.ToggleGroup;
  var Slider = exports.Slider;
  var Rgb = exports.Rgb;
  var RangeComponent = exports.RangeComponent;

  var oneDecimal = v => Number(v).toFixed(2);

  function Main(options, initialState) {
    Component.call(this, options, initialState);
    this.dataLoaded = this.dataLoaded.bind(this);
    this.state = initialState;
    this.components = {};

    fetch(initialState.dataUrl).then(data => data.json()).then(this.dataLoaded);
  }

  Main.prototype = Object.create(Component.prototype);

  Main.prototype.dataLoaded = function(geoJson) {
    this.components['toolbar'] = new Toolbar({
      id: 'toolbar',
      className: 'toolbar'
    });

    this.components['panel'] = new Panel(
      { id: 'panel', className: 'panel', label: 'World population' },
      { open: this.state.panelOpen }
    );

    this.components['togglePanel'] = new Toggle(
      { id: 'togglePanel', className: 'toggle togglePanel', label: ' ' },
      { active: this.state.panelOpen }
    );

    this.components['toggleDots'] = new ToggleGroup(
      {
        id: 'toggleDots',
        className: 'toggleGroup group toggleDots widget',
        label: 'Show population'
      },
      { active: this.state.dotsVisible }
    );

    this.components['radius'] = new Slider(
      {
        id: 'radius',
        className: 'radius slider widget',
        label: 'radius',
        range: [1, 10],
        round: oneDecimal
      },
      { value: this.state.style.radius }
    );

    this.components['opacity'] = new Slider(
      {
        id: 'opacity',
        className: 'opacity slider',
        label: 'Opacity',
        round: oneDecimal
      },
      { value: this.state.style.opacity }
    );

    this.components['weight'] = new Slider(
      {
        id: 'weight',
        className: 'weight slider widget',
        label: 'weight',
        range: [1, 10],
        round: oneDecimal
      },
      { value: this.state.style.weight }
    );

    this.components['map'] = new MapComponent(
      { id: 'map', className: 'map' },
      { data: geoJson }
    );

    this.components['colorGroup'] = new Group({
      className: 'color-group widget',
      label: 'Stroke'
    });

    this.components['color'] = new Rgb(
      {
        id: 'color',
        className: 'rgb color',
        label: 'Color',
        range: [0, 255],
        round: Math.round
      },
      {
        red: this.state.colorRGB[0],
        green: this.state.colorRGB[1],
        blue: this.state.colorRGB[2]
      }
    );

    this.components['fillColorGroup'] = new Group({
      id: 'fillColorGroup',
      className: 'fill-color-group widget',
      label: 'Fill'
    });

    this.components['fillColor'] = new Rgb(
      {
        id: 'fillColor',
        className: 'rgb color',
        label: 'Color',
        range: [0, 255],
        round: Math.round
      },
      {
        red: this.state.fillColorRGB[0],
        green: this.state.fillColorRGB[1],
        blue: this.state.fillColorRGB[2]
      }
    );

    this.components['fillOpacity'] = new Slider(
      {
        id: 'fillOpacity',
        className: 'fillOpacity slider',
        label: 'Opacity',
        round: oneDecimal
      },
      { value: this.state.style.fillOpacity / 10 }
    );

    this.mountElements();
    this.bindEvents();
    this.render(this.state);
  };

  Main.prototype.mountElements = function() {
    var map = this.components['map'];
    var panel = this.components['panel'];
    var toolbar = this.components['toolbar'];
    var togglePanel = this.components['togglePanel'];
    var toggleDots = this.components['toggleDots'];
    var radius = this.components['radius'];

    var fillColorGroup = this.components['fillColorGroup'];
    var colorGroup = this.components['colorGroup'];
    var color = this.components['color'];
    var opacity = this.components['opacity'];

    var fillOpacity = this.components['fillOpacity'];
    var weight = this.components['weight'];
    var fillColor = this.components['fillColor'];
    var container = this.$element;

    var panelNode = panel.node();
    var toolbarNode = toolbar.node();
    toggleDots.appendTo(panelNode);
    map.appendTo(container);
    panel.appendTo(container);
    toolbar.appendTo(container);
    togglePanel.appendTo(toolbarNode);
    radius.appendTo(panelNode);
    weight.appendTo(panelNode);

    var fillGroupNode = fillColorGroup.node();
    fillColorGroup.appendTo(panelNode);
    fillColor.appendTo(fillGroupNode);
    fillOpacity.appendTo(fillGroupNode);

    var colorGroupNode = colorGroup.node();
    colorGroup.appendTo(panelNode);
    color.appendTo(colorGroupNode);
    opacity.appendTo(colorGroupNode);
  };

  Main.prototype.mapLoaded = function() {
    return !!this.components['map'];
  };

  Main.prototype.bindEvents = function() {
    if (this.mapLoaded()) {
      this.components['togglePanel'].on('click', () => {
        this.setState({ panelOpen: !this.state.panelOpen });
      });

      this.components['toggleDots'].on('click', () => {
        this.setState({ dotsVisible: !this.state.dotsVisible });
      });

      this.components['map'].on('click', () => {
        this.setState({ panelOpen: false });
      });

      this.components['radius'].on('change', () => {
        var newRadius = this.components['radius'].value();
        var newStyle = Object.assign({}, this.state.style, {
          radius: newRadius
        });
        this.setState({ style: newStyle });
      });

      this.components['opacity'].on('change', () => {
        var newOpacity = this.components['opacity'].value();
        var newStyle = Object.assign({}, this.state.style, {
          opacity: newOpacity
        });
        this.setState({ style: newStyle });
      });

      this.components['fillOpacity'].on('change', () => {
        var newFillOpacity = this.components['fillOpacity'].value();
        var newStyle = Object.assign({}, this.state.style, {
          fillOpacity: newFillOpacity
        });
        this.setState({ style: newStyle });
      });

      this.components['weight'].on('change', () => {
        var newWeight = this.components['weight'].value();
        var newStyle = Object.assign({}, this.state.style, {
          weight: newWeight
        });
        this.setState({ style: newStyle });
      });

      this.components['color'].on('change', () => {
        var red = this.components['color'].value().red;
        var green = this.components['color'].value().green;
        var blue = this.components['color'].value().blue;
        this.setState({
          colorRGB: [red, green, blue]
        });
      });

      this.components['fillColor'].on('change', () => {
        var red = this.components['fillColor'].value().red;
        var green = this.components['fillColor'].value().green;
        var blue = this.components['fillColor'].value().blue;
        this.setState({
          fillColorRGB: [red, green, blue]
        });
      });
    }
  };

  Main.prototype.reduce = function(key, state) {
    switch (key) {
      case 'panel':
        return { open: state.panelOpen };
      case 'togglePanel':
        return { active: state.panelOpen };
      case 'toggleDots':
        var pop = state.dotsVisible ? 'Hide' : 'Show';
        return { active: state.dotsVisible, label: pop + ' population' };
      case 'radius':
        return { value: state.style.radius };
      case 'opacity':
        return { value: state.style.opacity };
      case 'fillOpacity':
        return { value: state.style.fillOpacity };
      case 'weight':
        return { value: state.style.weight };
      case 'map':
        return {
          style: Object.assign(state.style, {
            color: toHex(state.colorRGB),
            fillColor: toHex(state.fillColorRGB)
          }),
          panelOpen: state.panelOpen,
          dotsVisible: state.dotsVisible
        };

      case 'color':
        var red = state.colorRGB[0];
        var green = state.colorRGB[1];
        var blue = state.colorRGB[2];
        return { red, green, blue };

      case 'fillColor':
        var red = state.fillColorRGB[0];
        var green = state.fillColorRGB[1];
        var blue = state.fillColorRGB[2];
        return { red, green, blue };

      default:
        return state;
    }
  };

  Main.prototype.render = function(newState) {
    if (this.mapLoaded()) {
      Object.keys(this.components).map(name => {
        this.components[name].render(this.reduce(name, newState));
      });
    }
  };

  exports.Main = Main;
})(window.App);
