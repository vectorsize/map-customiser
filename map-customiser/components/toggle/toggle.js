(function(exports) {
  var Component = exports.Component;

  function Toggle(options, initialState) {
    Component.call(this, options, initialState);
  }

  Toggle.prototype = Object.create(Component.prototype);

  Toggle.prototype.setup = function(newState) {
    if (this.options.label) {
      this.$label = document.createElement('div');
      this.$label.className = 'toggle-label';
      this.$label.innerHTML = this.options.label;
      this.$element.append(this.$label);
    }
  };

  Toggle.prototype.render = function(newState) {
    var act = newState.active ? 'add' : 'remove';
    this.$element.classList[act]('active');
  };

  exports.Toggle = Toggle;
})(window.App);
