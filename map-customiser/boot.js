(function(exports) {
  var Main = exports.Main;

  document.addEventListener('DOMContentLoaded', function() {
    var fillColor = [54, 0, 196];
    var color = [255, 145, 140];
    var toHex = rgb => '#' + rgb.join();

    var initialData = {
      dataUrl:
        '/map-customiser/data/data.json',
      panelOpen: false,
      dotsVisible: true,
      minPopulation: 0,
      maxPopulation: 1,
      colorRGB: color,
      fillColorRGB: fillColor,
      style: {
        fillOpacity: 0.25,
        opacity: 1,
        radius: 5,
        weight: 1
      }
    };

    var app = new Main({ id: 'main', className: 'main' }, initialData);
    app.appendTo(document.querySelector('#app'));
  });
})(window.App);
