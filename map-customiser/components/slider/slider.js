(function(exports) {
  var Component = exports.Component;
  var scale = exports.scale;
  var identity = v => Number(v);

  function Slider(options, initialState) {
    Component.call(this, options, initialState);
  }

  Slider.prototype = Object.create(Component.prototype);

  Slider.prototype.setup = function() {
    var domain = [0, 1];
    var range = this.options.range || [0, 1];
    this.round = this.options.round || identity;
    this.scale = scale(domain, range);
    this.iScale = scale(range, domain);

    this.createLabel();
    this.createRange();
    this.createValue();
    if (!this.options.dumb) {
      this.$range.addEventListener('input', () =>
        this.setState({ value: this.round(this.scale(this.$range.value)) })
      );
    }
  };

  Slider.prototype.createLabel = function() {
    this.$label = document.createElement('span');
    this.$label.className = 'label';
    this.$label.innerHTML = this.options.label;
    this.$element.append(this.$label);
  };

  Slider.prototype.createRange = function() {
    this.$rangeContainer = document.createElement('div');
    this.$rangeContainer.className = 'range-container';
    this.$range = document.createElement('input');
    this.$range.type = 'range';
    this.$range.className = 'range';
    this.$range.min = 0;
    this.$range.max = 1;
    this.$range.step = 0.01;
    this.$element.append(this.$rangeContainer);
    this.$rangeContainer.append(this.$range);
  };

  Slider.prototype.createValue = function() {
    this.$value = document.createElement('span');
    this.$value.className = 'range-value';
    this.$rangeContainer.append(this.$value);
  };

  Slider.prototype.render = function(newState) {
    this.$range.value = this.iScale(newState.value);
    this.$value.innerHTML = newState.value;
  };

  Slider.prototype.emiter = function() {
    return this.$range;
  };

  Slider.prototype.value = function() {
    return Number(this.state.value);
  };

  exports.Slider = Slider;
})(window.App);
