(function(exports) {
  var Component = exports.Component;

  function Toolbar(options, initialState) {
    Component.call(this, options, initialState);
  }

  Toolbar.prototype = Object.create(Component.prototype);

  exports.Toolbar = Toolbar;
})(window.App);
